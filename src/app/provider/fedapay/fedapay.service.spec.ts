import { TestBed } from '@angular/core/testing';

import { FedapayService } from './fedapay.service';

describe('FedapayService', () => {
  let service: FedapayService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FedapayService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
