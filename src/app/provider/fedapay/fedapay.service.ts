import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    // 'Authorization': 'Bearer sk_sandbox_Urse_uj0QoOCfdMQUX5E4AoS',
    'Content-Type': 'application/json',
  })
};
@Injectable({
  providedIn: 'root'
})
export class FedapayService {

  url_feda = 'https://137.255.9.115/Ajout_Usager/ert';

  image: any;
  c_objet = {
    description: "Transaction for john.doe@example.com",
    amount: 2000,
    currency: { "iso": "XOF" },
    callback_url: "https://maplateforme.com/callback",
    customer: {
      firstname: "John",
      lastname: "Doe",
      email: "john.doe@example.com",
      phone_number: {
        number: "+22997808080",
        country: "bj"
      }
    }
  }

  constructor(private http: HttpClient) { }

  Demande(data):Observable<any>{
    return this.http.post(this.url_feda, data, httpOptions)
  }

  imag(){

    //  var file: File = val.target.files[0];
    var myReader: FileReader = new FileReader();

   myReader.onloadend = (e) => {
      // this.image = myReader.result;
      return of(myReader.result);
    
    }
  }

}
