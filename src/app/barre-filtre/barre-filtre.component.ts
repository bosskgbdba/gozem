import { Component, OnInit } from '@angular/core';
import { IdentificationComponent } from './../identification/identification.component';

@Component({
  selector: 'app-barre-filtre',
  templateUrl: './barre-filtre.component.html',
  styleUrls: ['./barre-filtre.component.scss'],
})
export class BarreFiltreComponent implements OnInit {
  var_filtre: boolean = true;

  constructor(private identite: IdentificationComponent) {}

  ngOnInit(): void {}

  reactionfiltre() {
    if (this.var_filtre === true) {
      this.var_filtre = false;
    } else {
      this.var_filtre = true;
    }
  }

  Details() {

      this.identite.var_detail = true;
   
  }
}
